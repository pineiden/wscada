from channels.routing import route, route_class
from .consumers import TestWSConsumer, Demultiplexer

testws_routing = [
    TestWSConsumer.as_route(path=r"^/ws/", attrs={'group':'testws'}),
    route_class(Demultiplexer, path='^/stream/?$')
]
