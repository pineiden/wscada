from channels.binding.websockets import WebsocketBinding
from .models import TestWS

class TestWSBinding(WebsocketBinding):
    model=TestWS
    stream='testws_stream'
    fields=['name','value']

    @classmethod
    def group_names(cls, instance):
        return ['testws_stream-updates']

    def has_permission(self, user, action, pk):
        return True