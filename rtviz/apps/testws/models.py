from django.db import models

# Create your models here.
class TestWS(models.Model):

    name = models.CharField(max_length=10, blank=True, null=True)
    value = models.IntegerField()

    class Meta:
        verbose_name = "test websocket"


