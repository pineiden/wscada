from django.db import models
from django.utils.translation import ugettext_lazy as _
from scigrammar.walnut import Walnut, genpaths
from scigrammar.brackets import parse2wnbank, WnAssemble, create_wn_all
from django.core.exceptions import ValidationError

import re


def parse_hand(wn_string):
    """Takes a string of cards and splits into a full hand."""
    if isinstance(wn_string, str):
        raise ValidationError(_("Invalid input for a Walnut instance"))
    return create_wn_all(wn_string)

class WalnutField(models.CharField):
    description = "A Walnut Tree field from Scigrammar"

    def __init__(self, max_length=300, *args, **kwargs):
        kwargs["max_length"]=max_length
        self.maxlen=max_length
        super(WalnutField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs=super(WalnutField, self).deconstruct()
        return name, path, args, kwargs

    def to_python(self, value):
        wn=create_wn_all(value)
        if isinstance(value, str):
            return value

        if value is None:
            return value

        return parse_hand(value)

    def deb_type(self, connection):
        return 'char(%s)' % self.maxlen


