from apps.sci_input.models import DataType, DataTypePath, DataTypeSocket

from channels.binding.websockets import WebsocketBinding


class DataTypeBinding(WebsocketBinding):
    model=DataType
    stream='datatype'
    fields=['name','csk','description','d3_graph']

    @classmethod
    def group_names(cls, instance):
        return ['datatype-updates']

    def has_permission(self, user, action, pk):
        return True
#Example:
#https://github.com/andrewgodwin/channels-examples/tree/master/databinding


class DataTypePathBinding(WebsocketBinding):
    model=DataTypePath
    stream='datatype_path'
    fields=['datatype','path']

    @classmethod
    def group_names(cls, instance):
        return ['datatype_path-updates']

    def has_permission(self, user, action, pk):
        return True


class DataTypeSocketBinding(WebsocketBinding):
    model=DataTypeSocket
    stream='datatype_socket'
    fields=['particular','standar']

    @classmethod
    def group_names(cls, instance):
        return ['datatype_socket-updates']

    def has_permission(self, user, action, pk):
        return True
