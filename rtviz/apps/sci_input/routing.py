# In routing.py
from channels.routing import route, route_class
from apps.sci_input.consumers import CSKConsumer,\
    ConsumerCSK, \
    ConsumerCSK_JSON, \
    Demultiplexer, \
    DTTPathConsumer,\
    DTTSocketConsumer

sci_routing = [
    CSKConsumer.as_route(path=r"^/datatype/", attrs={'group':'datatype'}),
    DTTPathConsumer.as_route(path=r"^/datatype_path/", attrs={'group':'datatype_path'}),
    DTTSocketConsumer.as_route(path=r"^/datatype_socket/", attrs={'group':'datatype_socket'}),
    route_class(Demultiplexer, path='^/stream/?$')
]
