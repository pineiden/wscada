from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.
from scigrammar.brackets import create_wn_all
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def check_csk(request):
    response_dict={
        'success':True,
        'graph':''
    }

    if request.is_ajax() and request.method=='POST':
        csk=request.POST.get('csk','')
        wn=create_wn_all(csk)
        d3graph=wn.getd3graph()
        print("D3:")
        print(d3graph)
        response_dict['graph']=d3graph
        response_dict['size']=wn.size
        print("JSON AJAX")
        print(response_dict)

    return JsonResponse(response_dict,safe=False)
