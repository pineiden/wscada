from django.http import HttpResponse
from channels.handler import AsgiHandler

from channels import Channel, Group
from channels.sessions import channel_session
from channels.auth import channel_session_user, channel_session_user_from_http

from channels.generic.websockets import JsonWebsocketConsumer

from scigrammar.brackets import create_wn_all
from networktools.colorprint import bprint, gprint, rprint


# add the bindings

from .bindings import DataTypeBinding, DataTypePathBinding, DataTypeSocketBinding

# Import model

from .models import DataType

from django.core import serializers

import simplejson as json

from datetime import datetime
"""
Groups:

What they do?

* admin: access to all features
* analist: visualize some information -> charts
* viewer: visualize few information -> charts

"""

# Connected to websocket.connect
class CSKConsumer(JsonWebsocketConsumer):
    # streamname='datatype'
    #channel_session_user = True
    strict_ordering=False
    channel_session = False
    groupname='datatype'

    def connect(self, message, **kwargs):
        multiplexer=kwargs['multiplexer']
        #self.message.reply_channel.send({"accept": True,'kk':'LOKPIYO'}
        rprint(message)
        gprint(message.content)
        rprint(message.channel)
        bprint(multiplexer)
        ##rprint("Enviando aprobacion")
        multiplexer.send({'accept':True})
        #super(CSKConsumer,self).connect(message,*args,**kwargs)
        #super(CSKConsumer,self).connect(message, multiplexer,**kwargs)
        ##self.raw_receive(message, multiplexer,**kwargs)
        #Group(self.groupname).discard(message.reply_channel)

    def connection_groups(self, **kwargs):
        """
        Return the list of allowed groups
        """
        return ["datatype","datatype-updates"]

    def disconnect(self, message, **kwargs):
        multiplexer=kwargs['multiplexer']
        bprint("Stream %s is closed" % multiplexer.stream)
        #Group(self.groupname).discard(message.reply_channel)


    def receive(self, content, **kwargs):
        """
        Called when a message is received with decoded JSON content
        """

        multiplexer=kwargs['multiplexer']

        gprint(multiplexer)
        bprint(content)
        response=None

        bprint(content)

        message=content


        command=message['command']
        checked=message['checked']
        idm=message['idm']

        rprint("Recibidos:")
        bprint(command)
        gprint(idm)
        bprint(checked)
        """
        'result' key is ever a string message with a sentence
        """
        if command=='check':
            rprint(command)
            msg= message['message']
            bprint("Mensaje")
            rprint(msg)
            wn=create_wn_all(msg['csk'])
            d3_graph=wn.getd3graph()
            message['message']['csk']=msg['csk']
            message['message']['graph']=d3_graph            
            rprint("A enviar a cliente:")
            gprint(content)
            res_msg=dict(
                idm=idm,
                command=command,
                message=message)

            multiplexer.send(res_msg)

        if command=='create' and checked==True:
            response="Instance Created!"
            bprint(content)
            datatype=message['message']['datatype']
            new_dtt=DataType(
                name=datatype['name'],
                csk=datatype['csk'],
                description=datatype['description'])
            new_dtt.save()
            res_msg=dict(
                idm=idm,
                command=command,
                message={'result':response})
            multiplexer.send(res_msg)

        elif command=='create' and checked=='False':
            response='Instance cannot be created because it\'s not checked'
            self.send({'msg':response})

        if command=='update' and checked==True:
            gprint("Updated:")
            bprint(content)
            datatype=content['message']['datatype']
            rprint(datatype)
            did=datatype['id']
            del datatype['id']
            dtt=object

            response=''
            try:
                fields= DataType._meta.fields
                name_fields=[]
                for field in fields:
                    print(field.name)
                    name_fields.append(field.name)
                dtt=DataType.objects.filter(pk=did).update(**datatype)
                response='Instance Updated!'
            except Exception as error:
                print("Error "+error)
                response='Instance not well Updated!'
                raise error
            res_msg=dict(
                idm=idm,
                command=command,
                message={'result':response})
            multiplexer.send(res_msg)
            rprint("Enviado con "+response)
            rprint(res_msg)

        if command=='delete':
            id=message['message']['id']
            response='Instance %s Deleted!' % id
            try:
                DataType.objects.filter(pk=id).delete()
            except Exception as error:
                raise error
            message=dict(
                idm=idm,
                command=command,
                message={'result':response})
            multiplexer.send(message)

        if command=='list':
            response="There is the list"
            queryset=DataType.objects.all()
            response="There is the instance by this name"
            bprint("Se entrega lista de elementos Datatype")
            if queryset:
                for instance in queryset:
                    print(instance)
                    ins_ser=serializers.serialize('json', [instance,])
                    inst_dict=json.loads(ins_ser[1:-1])
                    message=dict(
                        idm=idm,
                        command=command,
                        message=inst_dict)
                    multiplexer.send(message)
            else:
                response="There are no elements"
                message=dict(
                    idm=idm,
                    command=command,
                    message=response)
                multiplexer.send(message)

        if command=='get_id':
            id=message['message']['id']
            response="There is the instance id"
            queryset=DataType.objects.filter(pk=id)
            result_msg=dict()
            bprint(queryset)
            if queryset.exists():
                rprint("Existe un elemento")
                for instance in queryset:
                    ins_ser=serializers.serialize('json', [instance,])
                    # add graph
                    csk=instance.csk
                    wn=create_wn_all(csk)
                    graph=wn.getjsongraph()
                    inst_dict=json.loads(ins_ser[1:-1])
                    inst_dict['graph']=wn.dot.source
                    bprint("A ENVIAR A FRONT:")
                    rprint(inst_dict)
                    result_msg=dict(
                        idm=idm,
                        command=command,
                        message=inst_dict
                    )
                    multiplexer.send(result_msg)
            else:
                result_msg=dict(
                    idm=idm,
                    command=command,
                    message={'pk':0})
                multiplexer.send(result_msg)

        if command=='get_name':
            name=content['text']['name']
            response="There is the instance by this name"
            queryset=DataType.objects.filter(name=name)
            for instance in queryset:
                ins_ser=serializers.serialize('json', [instance,])
                self.send(ins_ser[1:-1])


    def disconnect(self, message, multiplexer, **kwargs):
        print("Stream %s is closed" % multiplexer.stream)

# Connected to websocket.connect
class DTTPathConsumer(JsonWebsocketConsumer):
    #channel_session_user = True
    strict_ordering=False
    channel_session = True

    def __init(*args,**kwargs):
        self.msg={}
        super(ConsumerCSK,self).__init__(*args,**kwargs)

    def connection_groups(self, **kwargs):
        """
        Return the list of allowed groups
        """
        return ["datatype","datatype-updates"]

    def raw_receive(self, message, **kwargs):
        bprint(message)
        rprint(message['text'])
        super(CSKConsumer, self).raw_receive(message, **kwargs)

    def receive(self, content, **kwargs):
        """
        Called when a message is received with decoded JSON content
        """
        bprint(content)
        response=None
        self.send(response)

# Connected to websocket.connect
class DTTSocketConsumer(JsonWebsocketConsumer):
    #channel_session_user = True
    strict_ordering=False
    channel_session = True

    def __init(*args,**kwargs):
        self.msg={}
        super(ConsumerCSK,self).__init__(*args,**kwargs)

    def connection_groups(self, **kwargs):
        """
        Return the list of allowed groups
        """
        return ["datatype","datatype-updates"]

    def raw_receive(self, message, **kwargs):
        bprint(message)
        rprint(message['text'])
        super(CSKConsumer, self).raw_receive(message, **kwargs)

    def receive(self, content, **kwargs):
        """
        Called when a message is received with decoded JSON content
        """
        bprint(content)
        response=None
        self.send(response)

# Connected to websocket.connect
@channel_session_user_from_http
def ws_add(message):
    # Accept connection
    message.reply_channel.send({"accept": True})
    # Add them to the right group
    Group("chat-%s" % message.user.username[0]).add(message.reply_channel)


# Connected to websocket.receive
@channel_session_user
def ws_message(message):
    Group("chat-%s" % message.channel_session['room']).send({
        "text": message['text'],
    })

# Connected to websocket.disconnect
@channel_session_user
def ws_disconnect(message):
    Group("chat-%s" % message.channel_session['room']).discard(message.reply_channel)

# Connected to websocket.connect
@channel_session_user
def ws_connect(message):
    # Accept connection
    message.reply_channel.send({"accept": True})
    # Work out room name from path (ignore slashes)
    room = message.content['path'].strip("/")
    # Save room in session and add us to the group
    message.channel_session['room'] = room
    Group("chat-%s" % room).add(message.reply_channel)


from channels.generic.websockets import WebsocketConsumer

class ConsumerCSK(WebsocketConsumer):

    # Set to True to automatically port users from HTTP cookies
    # (you don't need channel_session_user, this implies it)
    http_user = True
    # Set to True if you want it, else leave it out
    strict_ordering = False



    def connection_groups(self, **kwargs):
        """
        Called to return the list of groups to automatically add/remove
        this connection to/from.
        """
        return ["test"]

    def connect(self, message, **kwargs):
        """
        Perform things on connection start
        """
        # Accept the connection; this is done by default if you don't override
        # the connect function.
        self.message.reply_channel.send({"accept": True})

    def receive(self, text=None, bytes=None, **kwargs):
        """
        Called when a message is received with either text or bytes
        filled out.
        """
        # Simple echo
        rprint(text)
        self.send(text=text, bytes=bytes)


class ConsumerCSK_JSON(JsonWebsocketConsumer):

    # Set to True if you want it, else leave it out
    strict_ordering = False
    http_user = True
    channel_session = True

    def connection_groups(self, **kwargs):
        """
        Called to return the list of groups to automatically add/remove
        this connection to/from.
        """
        return ["test2"]

    def raw_receive(self, message, **kwargs):
        bprint(message['text'])
        super(ConsumerCSK_JSON, self).raw_receive(message, **kwargs)

    def receive(self, content, **kwargs):
        """
        Called when a message is received with decoded JSON content
        """
        # Simple echo
        gprint(content)
        self.send(content)

    def disconnect(self, message, **kwargs):
        """
        Perform things on connection close
        """
        pass

    # Optionally provide your own custom json encoder and decoder
    # @classmethod
    # def decode_json(cls, text):
    #     return my_custom_json_decoder(text)
    #
    # @classmethod
    # def encode_json(cls, content):
    #     return my_custom_json_encoder(content)


from channels.generic.websockets import  WebsocketDemultiplexer, JsonWebsocketConsumer


class Demultiplexer(WebsocketDemultiplexer):

    # Wire your JSON consumers here: {stream_name : consumer}
    consumers = {
        'datatype':CSKConsumer,
        'datatype_binding':DataTypeBinding.consumer,
        'datatype_binding_path':DataTypePathBinding.consumer,
        'datatype_binding_socket':DataTypeSocketBinding.consumer
    }

    # Optionally provide a custom multiplexer class
    # multiplexer_class = MyCustomJsonEncodingMultiplexer

    def connection_groups(self):
        return ['datatype-updates',
                'datatype_path-updates',
                'datatype_socket-updates']
