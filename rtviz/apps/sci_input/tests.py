from channels import Channel
from channels.test import ChannelTestCase

from sci_input.consumers import CheckCSKConsumer
from scigrammar.brackets import create_wn_all

class MyTests(ChannelTestCase):
    def test_a_thing(self):
        # This goes onto an in-memory channel, not the real backend.
        Channel("csk").send({"cks": "[hola|[a|[q|[(b|c)=(d|e)]]]=[f|[(g|h)]]]"})
        message=self.get_next_message("input", require=True)
        CheckCSKConsumer(message)
        result=self.get_next_message(message.reply_channel.name,require=True)
        csk=message['csk']
        wn=create_wn_all(csk)
        graph=wn.dot.source
        self.assertEqual(result['graph'],graph)
