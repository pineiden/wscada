from apps.sci_input.models import DataType
from django import forms


class DataTypeForm(forms.ModelForm):
    class Meta:
        model=DataType
        widgets = {
            'text': forms.TextInput(
                attrs={'id':'post-text',
                       'required':True,
                       'placeholder': "Creando CSK ..."})
        }

