from django.contrib import admin
from apps.charts.models import Chart, ChartCreator

# Register your models here.

@admin.register(Chart)
class ChartAdmin(admin.ModelAdmin):
    pass

@admin.register(ChartCreator)
class ChartCreator(admin.ModelAdmin):
    pass
