from django.apps import AppConfig


class ChartsConfig(AppConfig):
    name = 'charts'
    verbose_name='Charts'
