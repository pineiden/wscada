from django.http import HttpResponse
from channels.handler import AsgiHandler

from channels import Channel, Group
from channels.sessions import channel_session
from channels.auth import channel_session_user, channel_session_user_from_http

from channels.generic.websockets import JsonWebsocketConsumer

from channels.generic.websockets import WebsocketDemultiplexer, \
    JsonWebsocketConsumer



# import binding
from apps.datastorage.bindings import StationDataBinding
from apps.datastorage.models import StationData

from networktools.colorprint import bprint, gprint, rprint
from networktools.library import geojson2angularjson

import simplejson as json

from datetime import datetime

import math

class StationDataConsumer(JsonWebsocketConsumer):
    strict_ordering=False
    channel_session=True
    groupname='data-consumer'


    def connect(self, message, multiplexer,**kwargs):
        #multiplexer=kwargs['multiplexer']
        #self.message.reply_channel.send({"accept": True,'kk':'LOKPIYO'}
        rprint(message)
        gprint(message.content)
        rprint(message.channel)
        bprint(multiplexer)
        ##rprint("Enviando aprobacion")
        multiplexer.send({"status": "I just connected!"})
        print("2")
        multiplexer.send({"status": "I just connected 2!"})
        bprint("3")
        multiplexer.send({"status": "I just connected 3!"})

        #multiplexer.send({'accept':True})
        Group(self.groupname).add(message.reply_channel)
        rprint("Conexión aprobada")
        #super(CSKConsumer,self).connect(message,*args,**kwargs)
        #super(CSKConsumer,self).connect(message, multiplexer,**kwargs)
        ##self.raw_receive(message, multiplexer,**kwargs)


    def connection_groups(self, **kwargs):
        """
        Return the list of allowed groups
        """
        return ["data-consumer"]

    def disconnect(self, message, multiplexer, **kwargs):
        Group(self.groupname).discard(message.reply_channel)
        #multiplexer=kwargs['multiplexer']
        bprint("Stream %s is closed" % multiplexer.stream)

    def receive(self, content, multiplexer, **kwargs):
        """
        Called when a message is received with decoded JSON content
        """
        bprint(content)
        ang_json=geojson2angularjson(content)
        new_data=StationData(**new_value)
        new_data.save()
        response=None
        #multiplexer=kwargs['multiplexer']
        multiplexer.send(response, inmediatly=True)


class DataDemultiplexer(WebsocketDemultiplexer):

    # Wire your JSON consumers here: {stream_name : consumer}
    consumers = {
        'datashow':StationDataConsumer,
        'datastation_updates':StationDataBinding.consumer,
    }

    # Optionally provide a custom multiplexer class
    # multiplexer_class = MyCustomJsonEncodingMultiplexer

    def connection_groups(self):
        return ['data-updates',]
