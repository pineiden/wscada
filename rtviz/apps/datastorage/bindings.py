from apps.datastorage.models import StationData
from channels.binding.websockets import WebsocketBinding
from django.core import serializers
import simplejson as json

class StationDataBinding(WebsocketBinding):
    model=StationData
    stream='station-data'
    fields=[
        'source',
        'timestamp',
        'station_name',
        'data'        
    ]


    @classmethod
    def group_names(cls, instance):
        return ['data-updates']

    def has_permission(self, user, action, pk):
        return True

